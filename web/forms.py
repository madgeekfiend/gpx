from crispy_forms.bootstrap import FormActions
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit
from django.forms import models
from web.models import Workout


class GpxUploadForm(models.ModelForm):

    class Meta:
        model = Workout
        fields = ['content']

    helper = FormHelper()
    helper.form_class = 'form-horizontal'
    helper.layout = Layout(
        FormActions(
            Submit('upload_file', 'Import Workout', css_class='btn-primary')
        )
    )
