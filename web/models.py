from django.contrib.auth.models import User
from django.db import models
import gpxpy
from web.map import encode_coords


class Workout(models.Model):
    content = models.FileField(upload_to='gpx_uploads')
    user = models.ForeignKey(User)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    encoded_path = models.TextField(blank=True, null=True)

    def encode_path(self):
        # Encode the path using stuff
        # get the segments from file
        file = open(self.content.path, 'r')
        gpx = gpxpy.parse(file)
        # iterate through points and add to list as tuple
        # (lat, long)
        points = []
        for track in gpx.tracks:
            for segment in track.segments:
                for point in segment.points:
                    points.append((point.longitude, point.latitude))

        # now save it in the field and strip back slashes
        self.encoded_path = encode_coords(points).replace("\\\\","\\")
        #strip back slashes? wtf?!
        #file.close()


class Profile(models.Model):
    user = models.OneToOneField(User, max_length=50, blank=True, null=True)
    first_name = models.CharField(max_length=50, blank=True, null=True)
    last_name = models.CharField(max_length=50, blank=True, null=True)
    nickname = models.CharField(max_length=50, blank=True, null=True)


