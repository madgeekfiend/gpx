$(document).ready(function() {

    // Initialize modal
    //$('.modal').modal();

    $('#import-workout').click(function(e) {
        $('#import-modal').modal();
    });

    $('#begin-upload').click(function(e) {
       $('#import-modal').modal('hide');
        // Submit the form
        $('#upload-form').submit();
    });

});