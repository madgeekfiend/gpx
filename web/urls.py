from django.conf.urls import url
from web.views import HomePage, Dashboard

urlpatterns = [
    url(r'^$', HomePage.as_view(), name='home'),
    url(r'^dashboard/', Dashboard.as_view(), name='dashboard'),
]