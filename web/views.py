from os import path
from braces.views import LoginRequiredMixin
from django.contrib import messages
from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from web.models import Workout


class HomePage(TemplateView):
    template_name = 'web/index.html'


class Dashboard(LoginRequiredMixin, TemplateView):
    template_name = 'web/dashboard.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, {'workouts': Workout.objects.all()[:10]})

    def post(self, request, *args, **kwargs):
        if 'gpx_file' in request.FILES:
            f = Workout(content=request.FILES['gpx_file'], user=request.user)
            # Make sure it is the file we want
            extension = path.splitext(f.content.path)[1]
            if extension.lower() == '.gpx':
                f.save()
                f.encode_path()
                # create encoded path
                f.save()
                messages.success(request, 'Workout successfully imported')
            else:
                messages.error(request, 'Not a GPX file')
        else:
            messages.error(request, 'Unable to import file or no file present')

        return redirect('web:dashboard')
