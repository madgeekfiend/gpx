# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('web', '0002_auto_20150920_2111'),
    ]

    operations = [
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('first_name', models.CharField(max_length=50, blank=True, null=True)),
                ('last_name', models.CharField(max_length=50, blank=True, null=True)),
                ('nickname', models.CharField(max_length=50, blank=True, null=True)),
                ('user', models.OneToOneField(max_length=50, null=True, to=settings.AUTH_USER_MODEL, blank=True)),
            ],
        ),
    ]
