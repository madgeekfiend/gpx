# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0003_profile'),
    ]

    operations = [
        migrations.AddField(
            model_name='gpxfile',
            name='encoded_path',
            field=models.CharField(max_length=255, blank=True, null=True),
        ),
    ]
