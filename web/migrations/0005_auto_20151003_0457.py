# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0004_gpxfile_encoded_path'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gpxfile',
            name='encoded_path',
            field=models.TextField(null=True, blank=True),
        ),
    ]
