from django.apps import AppConfig


class WebAppConfig(AppConfig):

    name = 'web'

    def ready(self):
        import web.signals.handlers