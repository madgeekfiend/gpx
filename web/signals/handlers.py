from django.db.models.signals import post_delete
from django.dispatch import receiver
from web.models import Workout


# @receiver(post_save, sender=GpxFile)
# def gpxfile_postsave(sender, instance, created,  **kwargs):
#     # encode map path
#     instance.encode_path()

@receiver(post_delete, sender=Workout)
def gpxfile_post_delete(sender, instance, **kwargs):
    instance.content.delete(False)
