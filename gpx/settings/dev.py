from .base import *

DEBUG=True

#dev server must be first - so lame
INSTALLED_APPS.insert(0, 'devserver')

DEVSERVER_MODULES = (
    'devserver.modules.sql.SQLRealTimeModule',
    'devserver.modules.sql.SQLSummaryModule',
    'devserver.modules.profile.ProfileSummaryModule',

    # Modules not enabled by default
    'devserver.modules.ajax.AjaxDumpModule',
    #'devserver.modules.profile.MemoryUseModule',
    'devserver.modules.cache.CacheSummaryModule',
    #'devserver.modules.profile.LineProfilerModule',
)

INSTALLED_APPS += [
    'django_extensions',
]

DEVSERVER_AUTO_PROFILE = True